package log

import (
	"gitlab.com/zx42/go-deps-log/zap"
)

type (
	Logger interface {
		Error(msg string, args ...interface{})
		Info(msg string, args ...interface{})
		Debug(msg string, args ...interface{})
	}
)

var (
	defaultLogger Logger = zap.NewLogger()
	debugLogger   Logger = zap.NewDebugLogger()
)

func GetLogger() Logger {
	return defaultLogger
}

func GetDebugLogger() Logger {
	return debugLogger
}

// func RegisterLogger(lg Logger) {
// 	defaultLogger = lg
// }
