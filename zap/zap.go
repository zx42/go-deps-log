package zap

import (
	"go.uber.org/zap"
)

type (
	zapLoggerT struct {
		sugar *zap.SugaredLogger
	}
)

var (
	defaultLogger = NewLogger()
)

// func init() {
// 	log.RegisterLogger(defaultLogger)
// }

func NewLogger() *zapLoggerT {
	return newLogger(false)

	// // logger, err := zap.NewProduction(zap.AddCallerSkip(1))
	// logger, err := zap.NewDevelopment(zap.AddCallerSkip(1))
	// if err != nil {
	// 	panic(err)
	// }
	// // defer logger.Sync() // flushes buffer, if any

	// // func AddCallerSkip

	// zl := &zapLoggerT{
	// 	// sugar: logger.WithOptions(zap.AddCallerSkip(1)).Sugar(),
	// 	sugar: logger.Sugar(),
	// }
	// return zl
}

func NewDebugLogger() *zapLoggerT {
	return newLogger(true)

	// // logger, err := zap.NewProduction(zap.AddCallerSkip(1))
	// logger, err := zap.NewDevelopment(zap.AddCallerSkip(1))
	// if err != nil {
	// 	panic(err)
	// }
	// // defer logger.Sync() // flushes buffer, if any

	// // func AddCallerSkip

	// zl := &zapLoggerT{
	// 	// sugar: logger.WithOptions(zap.AddCallerSkip(1)).Sugar(),
	// 	sugar: logger.Sugar(),
	// }
	// return zl
}

func newLogger(dbg bool) *zapLoggerT {
	var (
		logger *zap.Logger
		err    error
	)

	if dbg {
		logger, err = zap.NewDevelopment(zap.AddCallerSkip(1))
	} else {
		logger, err = zap.NewProduction(zap.AddCallerSkip(1))
	}
	if err != nil {
		panic(err)
	}
	// defer logger.Sync() // flushes buffer, if any

	// func AddCallerSkip

	zl := &zapLoggerT{
		// sugar: logger.WithOptions(zap.AddCallerSkip(1)).Sugar(),
		sugar: logger.Sugar(),
	}
	return zl
}

func (zl *zapLoggerT) Error(msg string, args ...interface{}) {
	zl.sugar.Errorw(msg, args...)

	// err := zl.sugar.Sync()
	// if err != nil {
	// 	panic(err)
	// }
}

func (zl *zapLoggerT) Info(msg string, args ...interface{}) {
	zl.sugar.Infow(msg, args...)

	// err := zl.sugar.Sync()
	// if err != nil {
	// 	panic(err)
	// }
}

func (zl *zapLoggerT) Debug(msg string, args ...interface{}) {
	zl.sugar.Debugw(msg, args...)

	// err := zl.sugar.Sync()
	// if err != nil {
	// 	panic(err)
	// }
}
